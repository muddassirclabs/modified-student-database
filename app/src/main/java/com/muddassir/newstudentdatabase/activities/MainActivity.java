package com.muddassir.newstudentdatabase.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.muddassir.newstudentdatabase.R;
import com.muddassir.newstudentdatabase.adapters.MainAdapter;
import com.muddassir.newstudentdatabase.adapters.PagerAdapter;
import com.muddassir.newstudentdatabase.entities.Students;
import com.muddassir.newstudentdatabase.fragments.NavigationDrawerFragment;
import com.muddassir.newstudentdatabase.fragments.SpinnerFragment;
import com.muddassir.newstudentdatabase.util.DBController;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;

import static com.muddassir.newstudentdatabase.util.AppConstants.ADAPTER_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.ADD_REQUEST_CODE;
import static com.muddassir.newstudentdatabase.util.AppConstants.CLICK_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.CREATE_STUDENT;
import static com.muddassir.newstudentdatabase.util.AppConstants.DELETE_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.DELETE_SUCCESS;
import static com.muddassir.newstudentdatabase.util.AppConstants.DELETE_SUCCESS_MESSAGE;
import static com.muddassir.newstudentdatabase.util.AppConstants.DIALOG_MESSAGE;
import static com.muddassir.newstudentdatabase.util.AppConstants.DIALOG_TITLE;
import static com.muddassir.newstudentdatabase.util.AppConstants.EDIT_BUTTON;
import static com.muddassir.newstudentdatabase.util.AppConstants.EDIT_REQUEST_CODE;
import static com.muddassir.newstudentdatabase.util.AppConstants.EXTRACTION_SUCCESS;
import static com.muddassir.newstudentdatabase.util.AppConstants.FAILED_OPERATION;
import static com.muddassir.newstudentdatabase.util.AppConstants.FINAL_VALUE;
import static com.muddassir.newstudentdatabase.util.AppConstants.HOME;
import static com.muddassir.newstudentdatabase.util.AppConstants.INITIAL_VALUE;
import static com.muddassir.newstudentdatabase.util.AppConstants.LIST;
import static com.muddassir.newstudentdatabase.util.AppConstants.LOGOUT;
import static com.muddassir.newstudentdatabase.util.AppConstants.NO_ENTRY_FOUND_MESSAGE;
import static com.muddassir.newstudentdatabase.util.AppConstants.OBJECT_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.OPERATION_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.POSITION_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.SLEEP_TIME;
import static com.muddassir.newstudentdatabase.util.AppConstants.SORT_BY_NAME;
import static com.muddassir.newstudentdatabase.util.AppConstants.SORT_BY_ROLL;
import static com.muddassir.newstudentdatabase.util.AppConstants.STUDENT_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.TABS;
import static com.muddassir.newstudentdatabase.util.AppConstants.UPDATE_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.UPDATE_SUCCESS;
import static com.muddassir.newstudentdatabase.util.AppConstants.VIEW_ALL_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.VIEW_BUTTON;
import static com.muddassir.newstudentdatabase.util.AppConstants.VIEW_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.VIEW_SUCCESS;

/**
 * This is the Main Activity or Home page of the application.
 * This contain a navigation drawer for menu options and view pager with tab view for different
 * view.
 *
 * Update : Navigation icon animation added
 */

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, SpinnerFragment.OnDataPass,
        Serializable {

    transient ViewPager viewPager = null;
    MainAdapter adapter;
    transient ActionBar actionBar;
    transient ActionBar.Tab tab;
    transient Bundle bundle;
    transient private NavigationDrawerFragment mNavigationDrawerFragment;
    transient private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MainAdapter(this);
        initComponent();
        new DBInteractions().execute(null, VIEW_ALL_KEY);
    }

    /**
     * Initialize all the required components
     */
    public void initComponent() {
        // Initialize navigation drawer
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set a toolbar which will replace the action bar. - added for testing
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the drawer.
        /*mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));*/ // Uncomment after testing
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        // Setting up the bundle for fragments
        bundle = new Bundle();
        bundle.putSerializable(ADAPTER_KEY, adapter);

        // Set up view pager
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), bundle));

        // Setup action bar for tabbed view
        actionBar = getSupportActionBar();
        /*actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);*/ // uncomment this after testing
        actionBar.setTitle(TABS[LIST - 1]); // Added for testing purpose
        actionBar.show();

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                // show the given tab
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // hide the given tab
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
                // probably ignore this event
            }
        };

        // Creating Tabs - uncomment this after testing
        /*for (int i = 0; i < TABS.length; i++) {
            tab = actionBar.newTab()
                    .setText(TABS[i])
                    .setTabListener(tabListener);

            actionBar.addTab(tab);
        }*/ // Uncomment after testing

        // Changing tabs as per page
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //actionBar.setSelectedNavigationItem(position); // uncomment after testing
                actionBar.setTitle(TABS[position]); // For testing added
            }
        });
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    /**
     * Open respective page and set title of the action bar on navigation drawer item click
     *
     * @param number
     */
    public void onSectionAttached(int number) {
        switch (number) {
            case HOME:
                mTitle = getString(R.string.home);
                break;
            case CREATE_STUDENT:
                mTitle = getString(R.string.add_student);
                openAddStudent();
                break;
            case LOGOUT:
                mTitle = getString(R.string.logout);
                logout();
                break;
            default:
                break;
        }
    }

    // It will open the next activity(StudentOperations) to add a new student record
    void openAddStudent() {
        Intent intent = new Intent(this, StudentOperations.class);
        startActivityForResult(intent, ADD_REQUEST_CODE);
    }

    // Logout from the system and re-open the login activity
    private void logout() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    // Restoring Action Bar
    public void restoreActionBar() {
        actionBar = getSupportActionBar();
        /*actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);*/ //Uncomment after testing
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // This method will check for insertion or change of data in the called intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                bundle = data.getExtras();
                adapter.data.add((Students) bundle.get(OBJECT_KEY));
            }
        } else if (requestCode == EDIT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                bundle = data.getExtras();
                adapter.data.set(bundle.getInt(POSITION_KEY),
                        (Students) bundle.get(OBJECT_KEY));
            }
        }
        sortByRoll();
        adapter.notifyDataSetChanged();
    }

    @Override
    /**
     * This method identifies what type of sort request is made by the Spinner fragment
     */
    public void dataPass(int value) {
        if (value == SORT_BY_NAME) {
            sortByName();
        } else if (value == SORT_BY_ROLL) {
            sortByRoll();
        }
        adapter.notifyDataSetChanged();
    }

    // This method will sort the list view by Name
    public void sortByName() {
        Collections.sort(adapter.data, new Comparator<Students>() {
            @Override
            public int compare(Students lhs, Students rhs) {
                return (lhs.getName().compareToIgnoreCase(rhs.getName()));
            }
        });
    }

    // This method will sort the list view by Roll Number
    public void sortByRoll() {
        Collections.sort(adapter.data, new Comparator<Students>() {
            @Override
            public int compare(Students lhs, Students rhs) {
                return (lhs.getRollNo() - rhs.getRollNo());
            }
        });
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    /* This is the sub-class that inherits AsyncTask to perform database related operations in
         background.
         */
    public class DBInteractions extends AsyncTask<Object, Integer, Integer> {
        transient ProgressDialog progressDialog;
        transient Students studentObject;
        transient int objectPosition;

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle(DIALOG_TITLE);
            progressDialog.setMessage(DIALOG_MESSAGE);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        // This is the actual method that will perform database actions in background
        protected Integer doInBackground(Object[] params) {
            for (int i = INITIAL_VALUE; i < FINAL_VALUE; i++) {
                try {
                    Thread.sleep(SLEEP_TIME);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DBController dbController = new DBController(MainActivity.this);
            // For Extracting all values from database
            if (params[OPERATION_KEY].equals(VIEW_ALL_KEY)) {
                dbController.open();
                adapter.data = dbController.viewAllStudents();
                //adapter.data.addAll(data);
                dbController.close();
                if (adapter.data.size() > 0) {
                    return EXTRACTION_SUCCESS;
                }
            }
            // For deleting a value from database
            else if (params[OPERATION_KEY].equals(DELETE_KEY)) {
                objectPosition = (int) params[2];
                dbController.open();
                int result = dbController.deleteStudent((Students) params[STUDENT_KEY]);
                dbController.close();
                if (result > 0) {
                    return DELETE_SUCCESS;
                }
            }
            // For Extracting single value from database
            else if (params[OPERATION_KEY].equals(VIEW_KEY)) {
                dbController.open();
                studentObject = (Students) params[STUDENT_KEY];
                int rollNumber = studentObject.getRollNo();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollNo() > 0) {
                    return VIEW_SUCCESS;
                }
            }
            // For Extracting single value from database
            else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                objectPosition = (int) params[2];
                dbController.open();
                studentObject = (Students) params[STUDENT_KEY];
                int rollNumber = studentObject.getRollNo();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollNo() > 0) {
                    return UPDATE_SUCCESS;
                }
            }
            return FAILED_OPERATION;
        }

        @Override
        // This updates the progress as well as progress dialog
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);
        }

        @Override
        // This method checks for results of the background action
        protected void onPostExecute(Integer o) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            // If the operation performed is failed
            if (o == FAILED_OPERATION) {
                Toast.makeText(MainActivity.this,
                        NO_ENTRY_FOUND_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Extraction is successful
            else if (o == EXTRACTION_SUCCESS) {
                // Initially initialising it with list view
                sortByRoll();
                adapter.notifyDataSetChanged();
                mTitle = getTitle();
            }
            // For Deletion is successful
            else if (o == DELETE_SUCCESS) {
                adapter.data.remove(objectPosition);
                adapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this,
                        DELETE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
            }
            // For single Extraction is successful and open view activity(StudentOperations)
            else if (o == VIEW_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentOperations.class);
                intent.putExtra(CLICK_KEY, VIEW_BUTTON);
                intent.putExtra(OBJECT_KEY, studentObject);
                MainActivity.this.startActivity(intent);
            }
            // For single Extraction is successful and open edit activity(StudentOperations)
            else if (o == UPDATE_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentOperations.class);
                intent.putExtra(CLICK_KEY, EDIT_BUTTON);
                intent.putExtra(POSITION_KEY, objectPosition);
                intent.putExtra(OBJECT_KEY, studentObject);
                MainActivity.this.startActivityForResult(intent, EDIT_REQUEST_CODE);
            }
        }
    }
}
