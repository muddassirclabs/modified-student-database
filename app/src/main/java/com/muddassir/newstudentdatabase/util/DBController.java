package com.muddassir.newstudentdatabase.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.muddassir.newstudentdatabase.entities.Students;

import java.util.ArrayList;

import static com.muddassir.newstudentdatabase.util.DBConstants.ADDRESS_COLUMN;
import static com.muddassir.newstudentdatabase.util.DBConstants.ADDRESS_INDEX;
import static com.muddassir.newstudentdatabase.util.DBConstants.DB_NAME;
import static com.muddassir.newstudentdatabase.util.DBConstants.DB_VERSION;
import static com.muddassir.newstudentdatabase.util.DBConstants.DEPARTMENT_COLUMN;
import static com.muddassir.newstudentdatabase.util.DBConstants.DEPARTMENT_INDEX;
import static com.muddassir.newstudentdatabase.util.DBConstants.EMAIL_COLUMN;
import static com.muddassir.newstudentdatabase.util.DBConstants.EMAIL_INDEX;
import static com.muddassir.newstudentdatabase.util.DBConstants.NAME_COLUMN;
import static com.muddassir.newstudentdatabase.util.DBConstants.NAME_INDEX;
import static com.muddassir.newstudentdatabase.util.DBConstants.ROLL_COLUMN;
import static com.muddassir.newstudentdatabase.util.DBConstants.ROLL_INDEX;
import static com.muddassir.newstudentdatabase.util.DBConstants.TABLE_NAME;

public class DBController {
    DBHelper dbHelper;
    SQLiteDatabase database;
    Context context;

    public DBController(Context context) {
        this.context = context;
    }

    // This method will open connection to the database
    public void open() {
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        database = dbHelper.getWritableDatabase();
    }

    // This method will close connection to the database
    public void close() {
        database.close();
    }

    // This method will insert the data in the database
    public long insertStudent(Students students) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ROLL_COLUMN, students.getRollNo());
        contentValues.put(NAME_COLUMN, students.getName());
        contentValues.put(EMAIL_COLUMN, students.getEmail());
        contentValues.put(DEPARTMENT_COLUMN, students.getDepartment());
        contentValues.put(ADDRESS_COLUMN, students.getAddress());
        return database.insert(TABLE_NAME, null, contentValues);
    }

    // This method will update the data in the database
    public int updateStudent(Students students) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_COLUMN, students.getName());
        contentValues.put(EMAIL_COLUMN, students.getEmail());
        contentValues.put(DEPARTMENT_COLUMN, students.getDepartment());
        contentValues.put(ADDRESS_COLUMN, students.getAddress());
        return database.update(TABLE_NAME, contentValues, ROLL_COLUMN + "=?",
                new String[]{Integer.toString(students.getRollNo())});
    }

    // This method will delete the data from the database
    public int deleteStudent(Students students) {
        return database.delete(TABLE_NAME, ROLL_COLUMN + "=?",
                new String[]{Integer.toString(students.getRollNo())});
    }

    // This method will extract all data from the database
    public ArrayList<Students> viewAllStudents() {
        ArrayList<Students> allStudents = new ArrayList<>();
        int roll;
        String name, email, department, address;
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                roll = cursor.getInt(ROLL_INDEX);
                name = cursor.getString(NAME_INDEX);
                email = cursor.getString(EMAIL_INDEX);
                department = cursor.getString(DEPARTMENT_INDEX);
                address = cursor.getString(ADDRESS_INDEX);
                allStudents.add(new Students(roll, name, email, department, address));
                cursor.moveToNext();
            }
        }
        return allStudents;
    }

    // This method will extract single row from database
    public Students viewSingleStudents(int rollNumber) {
        Students tempStudent = null;
        int roll;
        String name, email, department, address;
        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME +
                " WHERE " + ROLL_COLUMN + "= " + rollNumber, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                roll = cursor.getInt(ROLL_INDEX);
                name = cursor.getString(NAME_INDEX);
                email = cursor.getString(EMAIL_INDEX);
                department = cursor.getString(DEPARTMENT_INDEX);
                address = cursor.getString(ADDRESS_INDEX);
                tempStudent = new Students(roll, name, email, department, address);
            }
        }
        return tempStudent;
    }
}
