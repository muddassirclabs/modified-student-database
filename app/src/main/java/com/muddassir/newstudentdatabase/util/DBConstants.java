package com.muddassir.newstudentdatabase.util;

public interface DBConstants {

    String DB_NAME = "clabs_db";
    String TABLE_NAME = "student_info";
    int DB_VERSION = 1;

    String ROLL_COLUMN = "roll_no";
    String NAME_COLUMN = "name";
    String EMAIL_COLUMN = "email";
    String DEPARTMENT_COLUMN = "department";
    String ADDRESS_COLUMN = "address";

    int ROLL_INDEX = 0;
    int NAME_INDEX = 1;
    int EMAIL_INDEX = 2;
    int DEPARTMENT_INDEX = 3;
    int ADDRESS_INDEX = 4;

}
