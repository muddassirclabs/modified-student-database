package com.muddassir.newstudentdatabase.adapters;

/**
 * Created by Muddassir on 23-Feb-15.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.muddassir.newstudentdatabase.fragments.StudentGridView;
import com.muddassir.newstudentdatabase.fragments.StudentListView;
import com.muddassir.newstudentdatabase.util.AppConstants;

import static com.muddassir.newstudentdatabase.util.AppConstants.GRID;
import static com.muddassir.newstudentdatabase.util.AppConstants.LIST;
import static com.muddassir.newstudentdatabase.util.AppConstants.MAX_PAGE;
import static com.muddassir.newstudentdatabase.util.AppConstants.TABS;

public class PagerAdapter extends FragmentStatePagerAdapter {
    Bundle bundle;

    public PagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;
    }

    @Override
    /**
     * This set up the fragment requested on sliding page
     */
    public Fragment getItem(int i) {
        Fragment fragment = null;
        if (i == (AppConstants.LIST - 1)) {
            fragment = new StudentListView();
        }
        if (i == (AppConstants.GRID - 1)) {
            fragment = new StudentGridView();
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return MAX_PAGE;
    }

    @Override
    /**
     * This returns the page title for page being viewed
     * @param position
     */
    public CharSequence getPageTitle(int position) {
        if (position == (AppConstants.LIST - 1)) {
            return TABS[LIST - 1];
        }
        if (position == (AppConstants.GRID - 1)) {
            return TABS[GRID - 1];
        }
        return null;
    }
}
