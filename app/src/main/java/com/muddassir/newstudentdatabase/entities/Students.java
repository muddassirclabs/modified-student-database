/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.muddassir.newstudentdatabase.entities;

import java.io.Serializable;


public class Students implements Serializable {

    //Data members for students
    private String name;
    private String address;
    private String department;
    private String email;
    private int rollNo;

    //constructor that will generate roll number as well
    public Students(int roll, String name, String email, String department, String address) {
        this.name = name;
        this.rollNo = roll;
        this.address = address;
        this.department = department;
        this.email = email;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public int getRollNo() {
        return rollNo;
    }

    public String getDepartment() {
        return department;
    }

    public String getEmail() {
        return email;
    }
}
