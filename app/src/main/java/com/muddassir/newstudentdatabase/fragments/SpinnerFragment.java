package com.muddassir.newstudentdatabase.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.muddassir.newstudentdatabase.R;

import static com.muddassir.newstudentdatabase.util.AppConstants.DROP_ITEMS;
import static com.muddassir.newstudentdatabase.util.AppConstants.SORT_BY_NAME;
import static com.muddassir.newstudentdatabase.util.AppConstants.SORT_BY_ROLL;

/**
 * Created by Muddassir on 24-Feb-15.
 */
public class SpinnerFragment extends Fragment {
    Spinner dropDown;
    int sortOption;
    OnDataPass onDataPass;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spinner, container, false);
        // Set Spinner(Drop Down Menu)
        dropDown = (Spinner) view.findViewById(R.id.spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, DROP_ITEMS) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    v = tv;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }
                return v;
            }
        };
        dropDown.setAdapter(arrayAdapter);

        // Setting Item Select Listeners for Spinner(Drop Down)
        dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == SORT_BY_ROLL) {
                    sortOption = SORT_BY_ROLL;
                } else if (position == SORT_BY_NAME) {
                    sortOption = SORT_BY_NAME;
                }
//                ((MainActivity)getActivity()).dataPass(sortOption);
                onDataPass.dataPass(sortOption);
                //dropDown.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onDataPass = (OnDataPass) activity;
        } catch (ClassCastException e) {
            Log.e(e.getMessage(), e.toString());
        }
    }

    /**
     * This interface is used to pass data to an activity.
     * That activity needs to implement it so as to receive data
     */
    public interface OnDataPass {
        public void dataPass(int value);
    }
}
