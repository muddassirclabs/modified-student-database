package com.muddassir.newstudentdatabase.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.muddassir.newstudentdatabase.R;
import com.muddassir.newstudentdatabase.activities.MainActivity;
import com.muddassir.newstudentdatabase.adapters.MainAdapter;

import static com.muddassir.newstudentdatabase.util.AppConstants.ADAPTER_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.DELETE_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.DIALOG_TITLE;
import static com.muddassir.newstudentdatabase.util.AppConstants.UPDATE_KEY;
import static com.muddassir.newstudentdatabase.util.AppConstants.VIEW_KEY;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class StudentListView extends Fragment {
    ListView listView;
    Button viewButton, editButton, deleteButton;
    int objectPosition;
    MainAdapter listAdapter;

    public StudentListView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_list_view, container, false);
        listView = (ListView) view.findViewById(R.id.student_list);

        // Call to AsyncTask to update the List View
        if (getActivity() instanceof MainActivity) {
            listAdapter = new MainAdapter(getActivity());
            if (!(this.getArguments() == null)) {
                listAdapter = (MainAdapter) getArguments().get(ADAPTER_KEY);
            }
            listView.setAdapter(listAdapter);
        }

        // Setting Item Click Listeners for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);
            }
        });

        return view;
    }

    /* This function handle the Button Click Listeners.
    1. View button will open the activity to show the details of the student
    2. Edit button will open the activity to edit the desired details of the student
    3. Delete button will call AsyncTask to remove the entry specified from the database
     */
    public void itemClick(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.activity_option_dialog);
        dialog.setTitle(DIALOG_TITLE);
        viewButton = (Button) dialog.findViewById(R.id.view);
        editButton = (Button) dialog.findViewById(R.id.edit);
        deleteButton = (Button) dialog.findViewById(R.id.delete);

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).new DBInteractions()
                            .execute(listAdapter.getItem(position), VIEW_KEY);
                }
                dialog.dismiss();
            }
        });
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectPosition = position;
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).new DBInteractions()
                            .execute(listAdapter.getItem(position), UPDATE_KEY, objectPosition);
                }
                listAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectPosition = position;
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).new DBInteractions()
                            .execute(listAdapter.data.get(position), DELETE_KEY, objectPosition);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
